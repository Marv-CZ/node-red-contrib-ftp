/**
 * Copyright 2015 Atsushi Kojo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

const ftp = require('ftp');
const fs = require('fs');

module.exports = function (RED) {
    'use strict';
    function FtpNode(n) {
        RED.nodes.createNode(this, n);
        const {
            credentials={},
        }=this
        const {
            host="localhost",
            port=21,
            secure=false,
            connTimeout=10000,
            pasvTimeout=10000,
            keepalive=10000,
            rejectUnauthorized,
            acceptSHA256,
        }=n
        const secureOptions={
            rejectUnauthorized,
        }
        if(acceptSHA256) secureOptions.checkServerIdentity=(_, c)=>
                    c.fingerprint256===acceptSHA256?undefined:new Error(`Cert ${c.fingerprint256} doesn't match required (${acceptSHA256})`)
        const {
            user="anonymous",
            password="anonymous@",
        }=credentials
        this.options = {
            host,
            port,
            secure,
            secureOptions,
            user,
            password,
            connTimeout,
            pasvTimeout,
            keepalive,
        };
    }

    RED.nodes.registerType('ftp', FtpNode, {
        credentials: {
            user: {
                type: "text",
            },
            password: {
                type: 'password',
            },
        }
    });

    function FtpInNode(n) {
        RED.nodes.createNode(this, n);
        this.ftp = n.ftp;
        this.operation = n.operation;
        this.filename = n.filename;
        this.localFilename = n.localFilename;
        this.ftpConfig = RED.nodes.getNode(this.ftp);

        if (this.ftpConfig) {
            var node = this;
            node.on('input', function (msg) {
                let conn = new ftp();
                let {
                    filename=msg.filename,
                    localFilename=msg.localFilename,
                }=msg
                this.sendMsg = async function (err, result) {
                    if (err) {
                        msg.error=err
                        node.error(err, msg);
                        node.status({ fill: 'red', shape: 'ring', text: 'failed' });
                        try{
                            conn.end()
                        }catch(e){
                            console.error(e)
                        }
                        return
                    }
                    node.status({});
                    let progress
                    if(node.operation=="get"){
                        progress=new Promise(resolve=>result.once("close", ()=>resolve()))
                        result.pipe(fs.createWriteStream(localFilename));
                    }else{
                        progress=Promise.resolve()
                        conn.end()
                    }
                    await progress
                    msg.payload=`${node.operation} successful`
                    msg.filename = filename;
                    msg.localFilename = localFilename;
                    node.send(msg);
                };
                conn.on('ready', function () {
                    switch (node.operation) {
                        case 'list':
                            conn.list(node.sendMsg);
                            break;
                        case 'get':
                            conn.get(filename, node.sendMsg);
                            break;
                        case 'put':
                            conn.put(localFilename, filename, node.sendMsg);
                            break;
                        case 'delete':
                            conn.delete(filename, node.sendMsg);
                            break;
                        case 'mkdir':
                            conn.mkdir(filename, true, node.sendMsg);
                            break;
                    }
                });
                conn.on('error', function(err) { 
                    node.error(err, msg);
                    node.status({ fill: 'red', shape: 'ring', text: err.message });
                    try{
                        conn.end()
                    }catch(e){
                        console.error(e)
                    }
                });
                //Clone object (object spread not present on node 10)
                const options=Object.assign({}, node.ftpConfig.options)
                const {auth}=msg
                if(auth){
                    options.user=auth.username
                    options.password=auth.password
                }
                conn.connect(options);
            });
        } else {
            this.error('missing ftp configuration');
        }
    }
    RED.nodes.registerType('ftp in', FtpInNode);
}
